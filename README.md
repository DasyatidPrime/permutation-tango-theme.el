# Permutation Tango theme for Emacs

<!-- [Commentary from permutation-tango-theme.el] -->
Permutation Tango is yet another dark Emacs theme based on the Tango
palette. Motivations included hate of gray backgrounds, low contrast,
and/or excessive bold, while wanting something more balanced-feeling
than the default Emacs colors.

Vanilla-plus might be a good descriptor for this theme. Many of the
colors are similar in relative placement to the default colors but
nudged and quantized into the Tango scheme. One interesting aspect is
that two additional hues (mint and rose) are more commonly used for
UI accents and cues, while most of the main palette is more commonly
used for text highlighting.

Note that while this is based on the Tango palette, it's not based on
any existing Tango-based Emacs theme, of which there predictably seem
to be many.

Known potential issues:

  - The mode line in Compile buffers may have poor contrast. See
    "FIXME(compile)" in the source if you want to try to fix this.

  - ‘hl-todo-keyword-faces’ is overridden to ensure the ‘hl-todo’
    face is used, but it also clobbers the standard keyword list with
    a reduced one. A user-set one will be preserved by Customize, but
    in that case it's your job to either use the ‘hl-todo’ face, pick
    matching colors, or live with the fruit salad.

Supported packages:

  - Emacs basic faces and Font Lock
  - ansi-color
  - Compile
  - Customize
  - hl-todo
  - Outline
  - Rainbow-delimiters
  - SLY
  - VC mode line
  - vc-annotate (age gradient)
<!-- [/Commentary] -->

<!-- See maint/rtt-el.el for why the above is (also) here. -->

<!--
    Local variables:
    mode: markdown
    fill-column: 72
    End:
 -->
