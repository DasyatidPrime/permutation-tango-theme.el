;;; rtt-el.el -- Ad-hoc personal functions for elisp maintenance         -*- lexical-binding: t -*-
;; SPDX-License-Identifier: CC-PDDC
;;; Commentary:
;; Completely ad-hoc. This is not a coherent package of anything. Functions are copied into
;; repositories as suitable for their maintenance.
;;; Code:

(require 'dash)
(require 'lisp-mnt)

;; It's a bit unclean to mechanically duplicate commentary in README text, but in 2022, a lot of
;; people visit repositories first and expect them to look nice (even though historically that's
;; not really what they're for). But we also need the text in the elisp file for packaging. So eh.

(defun rtt/el-update-readme-from-commentary ()
  "Find a README file with this file's Commentary section and update it."
  (interactive)
  (unless buffer-file-name
    (error "This buffer is not visiting a file"))
  (let ((expected-inclusion-name (file-name-nondirectory buffer-file-name))
        commentary readme-file-name)
    (setq commentary
          (--> (or (lm-commentary) (error "No commentary section found"))
               (replace-regexp-in-string "^;;; *Commentary:\\s-*\n" "" it)
               (replace-regexp-in-string "^;+ ?" "" it)
               (replace-regexp-in-string "\\`\n+" "" it)
               (replace-regexp-in-string "\n*\\'" "\n" it)
               (replace-regexp-in-string "`\\([^'\n]+\\)'" "‘\\1’" it)))
    (let ((readme-candidate-leaf-names '("README.md"))
          (probe-dir (file-name-directory buffer-file-name)))
      (setq readme-file-name
            (catch 'found
              (while t
                (dolist (leaf-name readme-candidate-leaf-names)
                  (let ((candidate (concat probe-dir leaf-name)))
                    (when (file-exists-p candidate)
                      (throw 'found candidate))))
                (let ((parent-dir (file-name-directory (directory-file-name probe-dir))))
                  (when (or (null parent-dir) (equal parent-dir probe-dir))
                    (error "No README file found")))))))
    (let* ((readme-buffer (find-file-noselect readme-file-name))
           (was-already-modified (buffer-modified-p readme-buffer)))
      (with-current-buffer readme-buffer
        (save-restriction
          (save-excursion
            (goto-char (point-min))
            (unless (-> (format "<!-- [Commentary from %s] -->" expected-inclusion-name)
                        (search-forward nil t))
              (error "%s does not request commentary from %s"
                     readme-file-name expected-inclusion-name))
            (forward-line +1)
            (let ((existing-start (point)))
              (unless (search-forward "<!-- [/Commentary] -->")
                (error "Unterminated commentary inclusion"))
              (delete-region existing-start (match-beginning 0))
              (goto-char existing-start))
            (insert commentary)))
        (unless was-already-modified
          (save-buffer))))))

;; Local variables:
;; fill-column: 99
;; End:

;;; rtt-el.el ends here
