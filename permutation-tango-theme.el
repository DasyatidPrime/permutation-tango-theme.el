;;; permutation-tango-theme.el --- Yet another dark Tango theme         -*- lexical-binding: t -*-

;; Copyright (C) 2022 Robin Tarsiger

;; Author: Robin Tarsiger <rtt@dasyatidae.com>
;; Maintainer: Robin Tarsiger <rtt@dasyatidae.com>
;; Created: 2022-01-01
;; Version: 1.1
;; Keywords: theme, dark
;; Package-Requires: ((emacs "24"))
;; SPDX-License-Identifier: GPL-3.0-only

;;; Commentary:

;; Permutation Tango is yet another dark Emacs theme based on the Tango
;; palette. Motivations included hate of gray backgrounds, low contrast,
;; and/or excessive bold, while wanting something more balanced-feeling
;; than the default Emacs colors.
;;
;; Vanilla-plus might be a good descriptor for this theme. Many of the
;; colors are similar in relative placement to the default colors but
;; nudged and quantized into the Tango scheme. One interesting aspect is
;; that two additional hues (mint and rose) are more commonly used for
;; UI accents and cues, while most of the main palette is more commonly
;; used for text highlighting.
;;
;; Note that while this is based on the Tango palette, it's not based on
;; any existing Tango-based Emacs theme, of which there predictably seem
;; to be many.
;;
;; Known potential issues:
;; 
;;   - The mode line in Compile buffers may have poor contrast. See
;;     "FIXME(compile)" in the source if you want to try to fix this.
;;
;;   - `hl-todo-keyword-faces' is overridden to ensure the `hl-todo'
;;     face is used, but it also clobbers the standard keyword list with
;;     a reduced one. A user-set one will be preserved by Customize, but
;;     in that case it's your job to either use the `hl-todo' face, pick
;;     matching colors, or live with the fruit salad.
;;
;; Supported packages:
;;
;;   - Emacs basic faces and Font Lock
;;   - ansi-color
;;   - Compile
;;   - Customize
;;   - hl-todo
;;   - Outline
;;   - Rainbow-delimiters
;;   - SLY
;;   - VC mode line
;;   - vc-annotate (age gradient)

;;; Code:

(deftheme permutation-tango
  "Yet another dark Emacs theme based on the Tango palette.")

(let (
      ;; Pure black and white.
      (Black "black") (White "white")

      ;; The official Tango palette (public domain).
      (Butter1 "#fce94f") (Butter2 "#edd400") (Butter3 "#c4a000")
      (Orange1 "#fcaf3e") (Orange2 "#f57900") (Orange3 "#ce5c00")
      (Chocolate1 "#e9b96e") (Chocolate2 "#c17d11") (Chocolate3 "#8f5902")
      (Chameleon1 "#8ae234") (Chameleon2 "#73d216") (Chameleon3 "#4e9a06")
      (SkyBlue1 "#729fcf") (SkyBlue2 "#3465a4") (SkyBlue3 "#204a87")
      (Plum1 "#ad7fa8") (Plum2 "#75507b") (Plum3 "#5c3566")
      (ScarletRed1 "#ef2929") (ScarletRed2 "#cc0000") (ScarletRed3 "#a40000")
      (Aluminium1 "#eeeeec") (Aluminium2 "#d3d7cf") (Aluminium3 "#babdb6")
      (Aluminium4 "#888a85") (Aluminium5 "#555753") (Aluminium6 "#2e3436")

      ;; Darker shades of the Tango colors, mostly for use in backgrounds.
      (ButterDark "#726010") (ChocolateDark "#372609") (ChameleonDark "#2d4e0d")
      (OrangeDark "#672e00") (ScarletRedDark "#700000")
      (AluminiumDark "#171a1b") (AluminiumExtraDark "#0b0d0d")

      ;; Intermediary pastel colors for gradients that shouldn't directly collide with other block
      ;; highlighting colors. ABC and DEF have warmer and cooler hue ranges.
      (PastelA "#ea9f7f") (PastelB "#e8ba76") (PastelC "#d4e788")
      (PastelD "#73c3d9") (PastelE "#cea0da") (PastelF "#d97bc1")

      ;; A few extra hues.
      (Mint1 "#55cf99") (Mint2 "#37936b") (Mint3 "#2a7252")
      (Rose1 "#cf558b") (Rose2 "#a6446f") (Rose3 "#87375b"))

  (custom-theme-set-variables
   'permutation-tango
   `(ansi-color-names-vector
     [,AluminiumDark ,ScarletRed1 ,Chameleon1 ,Butter1 ,SkyBlue1 ,Plum1 ,Mint1 ,Aluminium1])

   ;; Rainbow delimiters uses a 7-cycle. See below for the actual faces.
   `(rainbow-delimiters-max-face-count 7)
   `(rainbow-delimiters-outermost-only-face-count 0)

   ;; hl-todo uses a restricted keyword list and no separate coloration.
   '(hl-todo-keyword-faces '(("FIXME" . hl-todo) ("TODO" . hl-todo) ("XXX" . hl-todo)))

   ;; vc-annotate uses four gradients: scarlet-to-butter-ish, chocolate, plum, and aluminium. This
   ;; seems to do an interestingly good job of making recent changes stand out, at a glance, but it
   ;; could possibly use tweaking later. All the colors are somewhat tweaked around here, so we
   ;; don't use the palette names above even for the ones that still match exactly.
   `(vc-annotate-background ,AluminiumExtraDark)
   `(vc-annotate-color-map
     `((6 . "#e64632") (12 . "#e65232") (18 . "#ed6d25") (24 . "#ec9c1f") (30 . "#edb51f")
       (42 . "#e9b96e") (54 . "#d7a250") (66 . "#c19248") (78 . "#a9803f") (90 . "#97753e")
       (120 . "#ad7fa8") (150 . "#8f6592") (180 . "#75507b") (240 . "#684d6e") (300 . "#5a465f")
       (360 . "#888a85") (450 . "#7d807a") (540 . "#6e706b") (630 . "#61645f") (720 . "#555753")))
   `(vc-annotate-very-old-color "#414544"))

  ;; FIXME(compile)[2022-01-01]: There's a bit of a problem here with the way distant-foreground
  ;; settings work in the compilation-mode mode-line stuff. The Emacs defaults yield poor contrast.
  ;; 
  ;; One red herring here is that the distant-foreground stuff doesn't seem to show up in Customize
  ;; afterwards, but it _does_ show up via describe-face, so it's getting loaded. Another red
  ;; herring was that it seemed like it might be due to face inheritance, but that doesn't seem to
  ;; have been the problem either (I think).
  ;; 
  ;; The main part of the problem is that `compilation-mode-line-errors' assumes it can use
  ;; `compilation-error', `compilation-warning', and `compilation-info' on the mode line, even though
  ;; normal faces might not be suitable for the mode line. Then the reason the distant-foreground
  ;; workaround isn't triggering to get us out of it seems to just be that the distance threshold
  ;; is too low.
  ;; 
  ;; For now, in my personal configuration, I'm just hacking `compilation-mode-line-errors'
  ;; elsewhere to use the mode line faces. I tried setting `face-near-same-color-threshold', but it
  ;; doesn't seem to work well enough thus far; do we need the threshold to be on the "other side"
  ;; specifically for these faces? Does that even make any sense? (Yes, because the comparison
  ;; background color where we expect it to activate is different depending on the polarity. Ugh.)

  (custom-theme-set-faces
   'permutation-tango

   ;; Basic faces, with a lean toward mint and rose as UI accents.
   `(default ((t (:background ,Black :foreground ,Aluminium1))))
   `(cursor ((t (:background ,White))))
   `(error ((t (:foreground ,ScarletRed1 :distant-foreground ,ScarletRedDark :weight bold))))
   `(escape-glyph ((t (:foreground ,Mint1))))
   `(header-line ((t (:inherit nil :background ,Aluminium6 :foreground ,White :box nil))))
   `(highlight ((t (:background ,Mint3))))
   `(homoglyph ((t (:foreground ,Mint1))))
   `(isearch ((t (:background ,Rose3 :foreground ,White :underline t))))
   `(lazy-highlight ((t (:background ,Mint3))))
   `(link ((t (:foreground ,Rose1 :underline t))))
   `(link-visited ((t (:inherit link :foreground ,Mint2))))
   `(match ((t (:background ,SkyBlue3 :foreground ,White))))
   `(minibuffer-prompt ((t (:foreground ,Mint1))))
   `(nobreak-hyphen ((t (:foreground ,Mint1))))
   `(region ((t (:extend t :background ,Aluminium5))))
   `(secondary-selection ((t (:extend t :background ,Mint3))))
   `(shadow ((t (:foreground ,Aluminium4))))
   `(success ((t (:foreground ,Chameleon1 :distant-foreground ,ChameleonDark :weight bold))))
   `(trailing-whitespace ((t (:background ,ScarletRed3))))
   `(warning ((t (:foreground ,Orange2 :distant-foreground ,OrangeDark :weight bold))))

   ;; Decorations, mostly just quantized vanillish.
   `(line-number-major-tick ((t (:inherit line-number :weight bold))))
   `(line-number-minor-tick ((t (:inherit line-number :slant oblique))))
   `(mode-line ((t (:background ,Aluminium3 :foreground ,Black :box (:line-width -1 :style released-button)))))
   `(mode-line-inactive ((t (:inherit mode-line :background ,Aluminium6 :foreground ,Aluminium2 :box (:line-width -1 :color ,Aluminium5) :weight light))))
   `(window-divider ((t (:foreground ,Aluminium4))))
   `(window-divider-first-pixel ((t (:foreground ,Aluminium2))))
   `(window-divider-last-pixel ((t (:foreground ,Aluminium5))))

   ;; Widgets. I'm not sure what that red is doing in widget-button-pressed, honestly. Also, field
   ;; backgrounds are more distinguishable from region and other stuff this way.
   `(widget-button-pressed ((t (:foreground ,ScarletRed2))))
   `(widget-documentation ((t (:foreground ,Chameleon2))))
   `(widget-field ((t (:extend t :background ,ChocolateDark))))
   `(widget-single-line-field ((t (:inherit widget-field))))

   ;; Show Paren, similar to main UI above.
   `(show-paren-match ((t (:inherit highlight))))
   `(show-paren-match-expression ((t (:inherit show-paren-match :background ,ButterDark))))
   `(show-paren-mismatch ((t (:background ,ScarletRed2 :foreground ,White))))

   ;; Doc and comment faces being so similar but still distinct is a bit weird, but I tried making
   ;; doc and/or comment Chocolate1 (clash with vars), Chocolate3 (bad contrast), Orange2 (not bad
   ;; in itself but looks weird with so many similar shades next to each other)... and I think I
   ;; like the oblique comments better enough.

   ;; Font Lock, with many similarities to vanilla.
   `(font-lock-builtin-face ((t (:foreground ,Aluminium4))))
   `(font-lock-constant-face ((t (:foreground ,Butter2))))
   `(font-lock-comment-delimiter-face ((default (:inherit (font-lock-comment-face)))))
   `(font-lock-comment-face ((t (:foreground ,Chocolate2 :slant oblique))))
   `(font-lock-doc-face ((t (:foreground ,Chocolate2))))
   `(font-lock-function-name-face ((t (:foreground ,Plum1))))
   `(font-lock-keyword-face ((t (:foreground ,SkyBlue1))))
   `(font-lock-negation-char-face ((t (:foreground ,ScarletRed1))))
   `(font-lock-preprocessor-face ((t (:slant oblique :inherit (font-lock-builtin-face)))))
   `(font-lock-string-face ((t (:foreground ,Orange1))))
   `(font-lock-type-face ((t (:foreground ,Chameleon2))))
   `(font-lock-variable-name-face ((t (:foreground ,Chocolate1))))
   `(font-lock-warning-face ((t (:weight bold :foreground ,ScarletRed1 :inherit nil))))

   ;; Customize is significantly shifted around in hue.
   `(custom-button ((t (:background ,Aluminium3 :foreground ,Black :box (:line-width 2 :style released-button)))))
   `(custom-button-mouse ((t (:background ,Aluminium1 :foreground ,Black :box (:line-width 2 :style released-button)))))
   `(custom-button-pressed ((t (:background ,Aluminium3 :foreground ,Black :box (:line-width 2 :style pressed-button)))))
   `(custom-button-pressed-unraised ((t (:inherit custom-button-unraised :foreground ,Rose1))))
   `(custom-comment ((t (:background ,ChocolateDark))))
   `(custom-group-tag ((t (:inherit variable-pitch :foreground ,SkyBlue1 :weight bold :height 1.5))))
   `(custom-group-tag-1 ((t (:inherit variable-pitch :foreground ,SkyBlue1 :weight bold :height 1.5))))
   `(custom-state ((t (:foreground ,Chameleon2))))
   `(custom-variable-obsolete ((t (:foreground ,ButterDark))))
   `(custom-variable-tag ((t (:foreground ,Butter2 :weight bold))))
   `(custom-visibility ((t (:inherit link :foreground ,Aluminium4))))
   `(eieio-custom-slot-tag-face ((t (:foreground ,SkyBlue1))))

   ;; In Info, don't mark every third menu item, nor change fonts for quoted text.
   '(Info-quoted ((t (:inherit nil))))
   '(info-menu-star ((t nil)))

   ;; compilation-mode
   `(compilation-mode-line-exit ((t (:inherit compilation-info :distant-foreground ,Chameleon2 :foreground ,ChameleonDark))))
   `(compilation-mode-line-fail ((t (:inherit compilation-error :distant-foreground ,ScarletRed2 :foreground ,ScarletRedDark))))
   `(compilation-mode-line-run ((t (:inherit compilation-warning :distant-foreground ,Butter2 :foreground ,ButterDark))))
   `(compilation-info ((t (:inherit success))))
   `(compilation-error ((t (:inherit error))))
   `(compilation-warning ((t (:inherit warning))))

   ;; Rainbow delimiters are an exception to the usual palette; they're set up to use intermediary
   ;; hues exactly so they're less likely to visually slide in against other highlighting in a
   ;; confusing way. Aluminium2 at the beginning of the cycle is an exception, to provide a "pulse"
   ;; of "haven't you had enough levels of nesting yet".
   ;; 
   ;; TODO(next change): BCEADF isn't really the best order for these hues, is it...
   `(rainbow-delimiters-base-error-face ((t (:inherit rainbow-delimiters-base-face :foreground ,ScarletRed3))))
   `(rainbow-delimiters-base-face ((t (:inherit nil))))
   `(rainbow-delimiters-depth-1-face ((t (:inherit rainbow-delimiters-base-face :foreground ,Aluminium2))))
   `(rainbow-delimiters-depth-2-face ((t (:inherit rainbow-delimiters-base-face :foreground ,PastelB))))
   `(rainbow-delimiters-depth-3-face ((t (:inherit rainbow-delimiters-base-face :foreground ,PastelC))))
   `(rainbow-delimiters-depth-4-face ((t (:inherit rainbow-delimiters-base-face :foreground ,PastelE))))
   `(rainbow-delimiters-depth-5-face ((t (:inherit rainbow-delimiters-base-face :foreground ,PastelA))))
   `(rainbow-delimiters-depth-6-face ((t (:inherit rainbow-delimiters-base-face :foreground ,PastelD))))
   `(rainbow-delimiters-depth-7-face ((t (:inherit rainbow-delimiters-base-face :foreground ,PastelF))))

   ;; Outline uses a more gradient-y approach, since the visual distinction between adjacent levels
   ;; doesn't need to be that sharp and the faces are meant for longer stretches of text. The
   ;; Aluminium2 again provides a bit of a pulse.
   `(outline-1 ((t (:inherit nil :weight bold :foreground ,PastelA))))
   `(outline-2 ((t (:inherit nil :weight bold :foreground ,PastelB))))
   `(outline-3 ((t (:inherit nil :weight bold :foreground ,PastelC))))
   `(outline-4 ((t (:inherit nil :weight bold :foreground ,Aluminium2))))
   `(outline-5 ((t (:inherit nil :weight bold :foreground ,PastelD))))
   `(outline-6 ((t (:inherit nil :weight bold :foreground ,PastelE))))
   `(outline-7 ((t (:inherit nil :weight bold :foreground ,PastelF))))
   `(outline-8 ((t (:inherit nil :weight bold :foreground ,Aluminium2))))

   ;; Oblique/underline/bold for edited/added/out-of-date, then red underline for merge conflicts
   ;; and red strikethrough for removed files. The out-of-date and removed states are not as
   ;; observable in many contexts since VC's Git support doesn't seem to use them.
   `(vc-conflict-state ((t (:inherit vc-state-base :underline ,ScarletRed2))))
   `(vc-edited-state ((t (:inherit vc-state-base :slant oblique))))
   `(vc-locally-added-state ((t (:inherit vc-state-base :underline t))))
   `(vc-needs-update-state ((t (:inherit vc-state-base :weight bold))))
   `(vc-removed-state ((t (:inherit vc-state-base :strike-through ,ScarletRed2))))

   ;; SLY is less intensely colorful in some ways. That violet-red was a bit much.
   ;; 
   ;; A note on the MREPL colors: elsewhere, we usually use mint for prompts, but in this case,
   ;; many inputs and outputs will be inspectable and therefore in `sly-part-button-face'---and be
   ;; highlighted in `highlight' (which is mint above) when moused over, which gets visually joined
   ;; up to the prompts. So we swap rose in for the prompt, leaving mint for results.
   ;; 
   ;; (It's true that `link' and `highlight' already yield a rose/mint transition in hypertext, but
   ;; that has a more salutary cueing effect.)
   `(sly-action-face ((t (:foreground ,Butter2 :weight bold))))
   `(sly-db-catch-tag-face ((t (:inherit font-lock-constant-face))))
   `(sly-error-face ((t (:underline ,ScarletRed2))))
   `(sly-mode-line ((t (:weight bold))))
   `(sly-mrepl-note-face ((t (:inherit font-lock-comment-face))))
   `(sly-mrepl-output-face ((t (:foreground ,Aluminium2))))
   `(sly-mrepl-prompt-face ((t (:inherit nil :foreground ,Rose1))))
   `(sly-note-face ((t (:underline ,Butter3))))
   `(sly-part-button-face ((t (:inherit nil :foreground ,Mint1))))
   `(sly-warning-face ((t (:underline ,Orange2))))

   ;; hl-todo uses a single color per its configuration above.
   `(hl-todo ((t (:foreground ,ScarletRed1 :weight bold))))))

(provide-theme 'permutation-tango)

;; Note on `no-byte-compile': per the elisp manual, source files always take precedence for themes
;; anyway, so byte-compiling them is not just unnecessary but may be actively confusing.

;; Local variables:
;; no-byte-compile: t
;; fill-column: 99
;; End:

;;; permutation-tango-theme.el ends here
