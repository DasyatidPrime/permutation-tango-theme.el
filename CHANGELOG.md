# Changelog

This is a changelog for the Permutation Tango theme for Emacs. This file is based loosely on the
[Keep a Changelog][] conventions. The theme is likely to adhere to a *variant* of [Semantic
Versioning][] in which major versions are used for significant aesthetic changes, even-numbered
minor versions are used for adding support for more packages or otherwise filling in gaps, and
odd-numbered minor versions are reserved for experimentation.

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html

<!-- When editing this file, note the use of en dashes in the headings. -->

## 1.1 – ongoing

### Added

- Support for Outline faces, using two pastel groups broken up by aluminium.
- VC mode line faces, mostly style-based.

## 1.0.0 – 2022-01-01

Initial release.

<!--
    Local variables:
    mode: markdown
    fill-column: 99
    End:
 -->
